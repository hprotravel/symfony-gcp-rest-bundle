### Symfony Bundle built-on Guzzle for Google Cloud Platform REST APIs

Accessing to Google Cloud Platform Rest APIs using Service Account Credentials (Google's recommended way) 

For more information about authentication: https://cloud.google.com/speech/docs/common/auth

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```console
$ composer require hprotravel/symfony-gcp-rest-bundle
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require hprotravel/symfony-gcp-rest-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    GcpRestGuzzleAdapterBundle\GcpRestGuzzleAdapterBundle::class => ['all' => true],
];
```

### Step 3: Configuration
```yaml
gcp_rest_guzzle_adapter:
    clients:
        pubsub:
            email: 'test@test.com'
            private_key: "-----BEGIN PRIVATE KEY-----SDADAavaf...-----END PRIVATE KEY-----"
            scope: 'https://www.googleapis.com/auth/pubsub'
            project_base_url: 'https://pubsub.googleapis.com/v1/projects/test-project123/'

```

Note: Double quote for private_key. Single quote occur openssl_sign error

###### Accessing Service by container
```php
$pubSubClient = $container->get('gcp_rest_guzzle_adapter.client.pubsub_client');

$result = $pubSubClient->get(
    sprintf('topics/%s/subscriptions', 'test-topic')
);

var_dump((string)$result->getBody()->getContents());
```

###### Result
```php
string(113) "{
  "subscriptions": [
    "projects/test-project123/subscriptions/test_topicSubscriber"
  ]
}
"
```