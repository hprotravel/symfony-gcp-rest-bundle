## 0.4.0 (December 06, 2022)
  - Feature - CK-320 - Add max retries support

## 0.3.0 (April 13, 2022)
  - Feature - CK-163 - Upgraded gcp client to 0.3.0

## 0.2.3 (October 26, 2021)
  - Update README file for symfony best practice

## 0.2.2 (October 26, 2021)
  - Change requred gcp rest client repo with bitbucket

## 0.2.1 (October 26, 2021)
  - Remove -compass suffix from package name

## 0.2.0 (October 26, 2021)
  - Move all files to src directory
  - Change package name with hprotravel-compass/symfony-gcp-rest-bundle

## 0.1.2 (December 03, 2020)
  - use namespace and short class name
  - downgrade phpunit version
  - compatible with symfony 4 and symfony 5

## 0.1.1 (April 04, 2019)
  - Change branch alias 1.0.x-dev to 0.1.x-dev

## 0.1.0 (April 03, 2019)
  - upgrade symfony requirements version 2 to 3
  - Package fixes
  - minor package fix
  - initial commit

